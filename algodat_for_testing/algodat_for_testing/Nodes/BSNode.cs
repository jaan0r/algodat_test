﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    class BSNode
    {
        BSNode left;
        BSNode right;
        int element;
        int level = 0;

        #region Constructor
        public BSNode(int Element=0)
        {
            element = Element;
        }
        #endregion

        #region Properties
        public int Level
        {
            get { return level; }
            set { level = value; }
        }
        public BSNode Left
        {
            get { return left; }
            set { left = value; }
        }

        public BSNode Right
        {
            get { return right; }
            set { right = value; }
        }

        public int Element
        {
            get { return element; }
            set { element = value; }
        }

        #endregion
    }

}
