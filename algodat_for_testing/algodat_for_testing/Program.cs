﻿using algodat_for_testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algodat_for_testing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hallo und Herzlich willkommen");

            //BinSearchTree tree = new BinSearchTree();

            //tree.inserttest(22);
            //tree.inserttest(1);
            //tree.inserttest(3);
            //tree.inserttest(2);
            //tree.inserttest(25);
            //tree.inserttest(33);
            //tree.inserttest(27);
            //tree.inserttest(43);
            //tree.inserttest(42);
            //tree.inserttest(46);


            //// BSNode test = tree.getResult(tree.Root,33);
            //tree.Print();
            //Console.WriteLine("-------------------------------------------");
            //tree.BSTDelete(tree.Root, 33);
            //tree.Print();
            //// Console.WriteLine(test.Element);
            //tree.Print();

            AVLTree avltree = new AVLTree();

            avltree.Add(22);
            avltree.Add(1);
            avltree.Add(3);
            avltree.Add(2);
            avltree.Add(25);
            avltree.Add(33);
            avltree.Add(27);
            avltree.Add(43);
            avltree.Add(42);
            avltree.Add(46);

            avltree.DisplayTree();
            Console.WriteLine("-------------------------------------------");
            avltree.Delete(33);
            avltree.DisplayTree();

            Console.ReadKey();


        }

       
    }
}
