﻿using System;
namespace algodat_for_testing
{
    public class AVLNode : BSNode
    {
        int height;
         AVLNode _left;
         AVLNode _right;
        int _element;

        public int Height
        {
            get { return height; }
            set { height = value; }
        }
        public AVLNode(int ele)
        {
            element = ele;
        }
        new public AVLNode Left
        {
            get { return _left; }
            set { left = value; }
        }
        new public AVLNode Right
        {
            get { return _right; }
            set { right = value; }
        }

        public int Element
        {
            get { return _element; }
            set { _element = value; }
        }

    }


}
