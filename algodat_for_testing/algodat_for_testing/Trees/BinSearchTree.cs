﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    abstract class BinSearchTree : ISetSorted
    {
        BSNode root;
        public BSNode Root
        {
            get; set;
        }

        private BSNode getResult(BSNode root,int value)
        {
            if (root == null)
                throw new Exception("critical fail");
            else if (value < root.Element)
            {
                
                BSNode BSleft = getResult(root.Left, value);
                if (BSleft.Element == value)
                    return BSleft;
                else
                getResult(root.Left, value);
                
            }
            else if (value > root.Element)
            {
                BSNode BSright = getResult(root.Right, value);
                if (BSright.Element == value)
                    return BSright;
                else
                getResult(root.Right, value);
            }
            return null;
                
        }

        public bool Search(BSNode root,int value)
        {
            if (root == null)
                return false;
            else if (value < root.Element)
                return Search(root.Left, value);
            else if (value > root.Element)
                return Search(root.Right, value);

            return true;
        }


         public bool Insert(BSNode root, int value)
        {
            if (root == null)
                //empty Tree
            {
                BSNode node = new BSNode(value);
                this.root = node;
            }
            // Left
            else if (value < root.Element)
            {
                if (root.Left != null)
                {
                    root.Level++;
                    Insert(root.Left,value);
                }
                else
                {
                    root.Left = new BSNode(value);
                    return true;
                }
            }
            //right
            else if (value > root.Element)
            {
                if (root.Right != null)
                {
                    root.Level++;
                    Insert(root.Right,value);
                }
                else
                {
                    root.Right = new BSNode(value);
                    return true;
                }
            }
            return false;
        }

        public bool Delete(BSNode node, int element)
        {
          if(root == null)
            return false;
            
            if (node.Element < element)
                return Delete(node.Left, element);
            if (node.Element > element)
                return Delete(node.Right, element);

            if(element == node.Element)
            {
                // No Leafs
                if (node.Left == null && node.Right == null)
                {
                    Root = null;
                    return true;
                }
                // No Left Leaf
                else if (node.Left == null)
                {
                    BSNode del = node;
                    node = node.Right;
                    del = null;
                    return true;
                }
                // No right Leaf
                else if (node.Right == null)
                {
                    BSNode del = node;
                    node = node.Left;
                    del = null;
                    return true;

                }
                //both leafs
                else
                {
                    BSNode del = node;
                    node = node.Right;
                    node.Right = null;
                    del = null;
                    return true;

                }
                }
            return false;
            }
        public void Deletenode(int x)
        {

            Delete(root, x);
        }
        public void Print(BSNode node)
        {
            foreach(BSNode a in )
            for (int i = 0; i < node.Level; i++)
            {
                Console.WriteLine("---");
                Console.Write(node.Element);
            }
        }


          
        }



    }

        }


     

